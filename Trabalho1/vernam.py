#!/usr/bin/env python3

#############################################################
#                                                           #
#                                                           #
#      Trabalho 1 de Segurança e Auditoria de Sistemas      #
#      Autor: Vinícius Sandri Diaz                          #
#                                                           #
#                                                           #
#      data da última modificação: 26/03/18                 #
#      Implementação:                                       #
#                     Cifra de Vernam                       #
#                                                           #
#                                                           #
#                                                           #
#                                                           #
#############################################################


### Comandos

#     python cesar.py -c 5 c_text.txt saida.txt^C
     
####     


import numpy as np
import collections
import sys
from random import *

upper = range(65, 91)
lower = range(97, 123)
digit = range(48, 58)

rotation = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'

criptografada = ''
descriptografada = ''


def criptografaVernam(entrada, key, criptografada):
    
    key = ''
    for c in entrada:     
        
        if c != ' ' and ((ord(c) in upper) or (ord(c) in lower) or (ord(c) in digit)):
                        
            swp = rotation.find(c)  
            tmp = randrange(0,(len(rotation)-1),1)
            
            if swp != tmp:

                key += rotation[tmp]
                
            elif swp == tmp == (len(rotation)-1):
                 key += rotation[tmp - 1]
                 
            elif swp == tmp <= (len(rotation)-1):
                 key += rotation[tmp + 1]     
     
        else:
            key +=  c
    
    return key 


def criptografaVernamChaveExistente(entrada, key, criptografada):
    
    i = 0    
    for c in entrada:     
        
        if c != ' ' and ((ord(c) in upper) or (ord(c) in lower) or (ord(c) in digit)):
                        
            swp = rotation.find(c)
            tmp = rotation.find(key[i])             

            criptografada += rotation[((swp^tmp) % len(rotation))]
           
     
        else:
            criptografada +=  c
            
        i += 1   
     

    return criptografada 


def descriptografaVernam(entrada, key, descriptografada):
    
    i = 0    
    for c in entrada:   
        if c != ' ' and ((ord(c) in upper) or (ord(c) in lower) or (ord(c) in digit)):
                        
            swp = rotation.find(c) 
            tmp = rotation.find(key[i])

            descriptografada += rotation[((swp^tmp) % len(rotation))]

     
        else:
            descriptografada +=  c
            
        i += 1    

    
    return descriptografada
    

def main():
    
    entrada = ''
    key = ''
  
    
    if len(sys.argv) == 5:
        opt = sys.argv[0]
        escolha = sys.argv[1]
        pathKey = sys.argv[2]
        pathEntrada = sys.argv[3]
        pathSaida = sys.argv[4]
        
        tempKey = open(pathKey, 'r')
        tempEntrada = open(pathEntrada, 'r')
        saida = open(pathSaida, 'w')
        
        
        for linhaKey in tempKey:
            for k in linhaKey:
                key += k
        
        for linha in tempEntrada:
            for c in linha:
                entrada += c
        
        
        
        if opt=='Vernam.py':
            if escolha == '-c':
                print("Criptografia")
                a =criptografaVernamChaveExistente(entrada, key, criptografada)
                      
            elif escolha == '-d':
                print("Descriptografia")
                a = descriptografaVernam(entrada, key, descriptografada)
                
            elif escolha == '-k':
                print("Gerando Chaves")
                a = criptografaVernam(entrada, key, descriptografada)    
        
        saida.writelines(a)
        saida.close()
        
    
        
    else:
        print("Errou feio, errou rude!")

        

if __name__ == "__main__":
    main()
