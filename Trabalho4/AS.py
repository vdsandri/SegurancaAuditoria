#!/usr/bin/env python3

#############################################################
#                                                           #
#                                                           #
#      Trabalho 4 de Segurança e Auditoria de Sistemas      #
#      Autor: Vinícius Sandri Diaz                          #
#                                                           #
#                                                           #
#      data da última modificação: 29/05/18                  #
#      Implementação:                                       #
#                     Servidor AS                           #
#                                                           #
#                                                           #
#                                                           #
#############################################################


import socket
import sys
from _thread import *
from Crypto.Cipher import DES3
import time
from time import strftime
import numpy as np
import hashlib


HOST = ''
PORT = 6666
BUFFER_SIZE = 1024
CONEXAO = 10
salt = 895807183804302924969748361977
listUser = {}
listKTGS = {}


def hashGenerator(entrada, salt):

    hash = entrada + str(salt).encode()
    base = hashlib.sha256(hash).hexdigest()
    return base


def usuarioList():

    listUser.clear()
    try:
        userFile = open("idc.txt", 'r')
        for line in userFile:
            campo = line.split(",")
            listUser[campo[0]] = campo[1]

        time.sleep(0.05)
        userFile.close()
    except Exception as e:
        print("Erro ao abrir o arquivo idc.txt: ", str(e))


def createCipher(usuario):
    try:
        key = listUser.get(usuario)
        key32 = key.ljust(24)[:24]
        iv = key.ljust(16)[35:43]
        cipher = DES3.new(key32, DES3.MODE_CFB, iv)
        return cipher
    except Exception as e:
        print("Erro ao gerar a cifra: ", str(e))


def cipherKCTGS(usuario):

    try:
        listKTGS.clear()
        time.sleep(0.05)
        npk = str(np.random.bytes(16))
        key = hashGenerator(npk.encode(), salt)
        listKTGS[usuario] = key

    except Exception as e:
        print("Erro ao gravar o arquivo kctgs.txt: ", str(e))


def encryptText(mensagem, cipher):
    cipher = cipher
    space = '        '
    text = mensagem
    plaintext = space + text
    msg = cipher.encrypt(plaintext.encode(encoding='utf_8'))
    print("encrypt: " + str(msg))
    return msg


def decryptText(mensagem, cipher):
    cipher = cipher
    msg = cipher.decrypt(mensagem)
    print("Msg decrypt: ", msg[8:])
    return msg[8:]


def serverThread(conn):
    msg = "Bem vindo ao Servidor AS"
    conn.send(msg.encode())

    while True:
        try:
            usuarioList()
            data = conn.recv(BUFFER_SIZE)
            usuario = data.decode("utf-8")

            if usuario in listUser:
                reply = "OK. Usuario Válido! " + usuario
                cipher = createCipher(usuario)
                cipherKCTGS(usuario)
                time.sleep(0.1)
                conn.send(reply.encode())
                msga = conn.recv(BUFFER_SIZE)
                if len(msga) > 0:
                    a = decryptText(msga, cipher)
                    line = str(a)
                    resp = geraResposta(usuario, line, cipher)
                    conn.send(resp)
                    time.sleep(0.05)
                    ticket = geraTicket(usuario, line)
                    conn.send(ticket)
                    print()
                    break

            else:
                print("Usuario inexistente!")

            if not data:
                print("data == 0 no AS")
            break

        except Exception as e:
            print("Timout AS: ", str(error))
            break

    time.sleep(0.05)
    conn.close()


def geraResposta(usuario, msg, cipher):

    try:
        if usuario in listKTGS:
            nbr = listKTGS[usuario]
            m = msg.split("b'")[1]
            m = m.replace(m[len(m)-1], "", 1)
            m = m.split(",")
            m2 = str(nbr) + "," + m[2]
            print("Resposta AS: ", m2)
            resp = encryptText(m2, cipher)
        return resp

    except Exception as e:
        print("Erro ao gerar kctgs", str(e))


def geraTicket(user, msg):

    listASTGS = {}
    usuario = 'astgs'
    try:
        userFile = open("ktgs.txt", 'r')
        for line in userFile:
            campo = line.split(",")
            listASTGS[campo[0]] = campo[1]

        time.sleep(0.05)
        userFile.close()
        try:
            if usuario in listASTGS:
                key = listASTGS[usuario]
                key32 = key.ljust(24)[:24]
                iv = key.ljust(16)[35:43]
                cipher = DES3.new(key32, DES3.MODE_CFB, iv)
                m = msg.split("b'")[1]
                m = m.replace(m[len(m)-1], "", 1)
                m = m.split(",")
                tempo = strftime("%Y:%m:%d:%H:%M", time.gmtime())
                m2 = user + "," + m[1] + "," + tempo + "," + str(listKTGS[user])
                print("Msg Ticket: ", m2)
                resp = encryptText(m2, cipher)
            return resp
        except Exception as e:
            print("Erro ao gerar cifra ktgs.txt no geratick: ", str(e))

    except Exception as e:
            print("Erro ao abrir o arquivo ktgs.txt: ", str(e))


def main():

    tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print("Socket criado")

    try:
        tcp.bind((HOST, PORT))
    except socket.error:
        print("Binding failed")
        sys.exit

    print("Socket  bounded")
    tcp.listen(CONEXAO)

    print("Socket está pronto")
    print("Bem vindo ao Servidor AS")
    while 1:
        conn, addr = tcp.accept()
        print("Conectado com: " + addr[0] + ":" + str(addr[1]))
        print()
        start_new_thread(serverThread, (conn, ))

    conn.close()
    tcp.close()


if __name__ == "__main__":
    main()
