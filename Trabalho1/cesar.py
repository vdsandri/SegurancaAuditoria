#!/usr/bin/env python3

#############################################################
#                                                           #
#                                                           #
#      Trabalho 1 de Segurança e Auditoria de Sistemas      #
#      Autor: Vinícius Sandri Diaz                          #
#                                                           #
#                                                           #
#      data da última modificação: 23/03/18                  #
#      Implementação:                                       #
#                     Cifra de Cesar                        #
#                                                           #
#                                                           #
#                                                           #
#############################################################

#Exemplos de comandos:
    
#   python cesar.py -c  -k 5 c_text2.txt  saida.txt


import numpy as np
import collections
import sys


upper = range(65, 91)
lower = range(97, 123)
digit = range(48, 58)

rotation = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'

criptografada = ''
descriptografada = ''
frequenciaTexto = collections.Counter()


def cifradorDeCesar(entrada, key, criptografada):
    for c in entrada:
        swp = rotation.find(c)
        
        if c != ' ' and ((ord(c) in upper) or (ord(c) in lower) or (ord(c) in digit)):
            
            criptografada += rotation[((swp + key) % len(rotation))]
     
        else:
            criptografada +=  c
        
    print("Msg Original:         ", entrada)
    print("Msg criptografada:    ", criptografada)
   

    return criptografada
    

def decifradorDeCesar(entrada, key, descriptografada):
    
    for c in entrada:
        swp = rotation.find(c)
       
        if c != ' ' and ((ord(c) in upper) or (ord(c) in lower) or (ord(c) in digit)):
            
            descriptografada += rotation[((swp - key) % len(rotation))]
                        
        else:
            descriptografada +=  c
    
    return descriptografada

def frequenciaChar(entrada):
    a = entrada.lower()
    frequenciaTexto.clear()

    for c in a:
        if c in a and c != ' ' and ((ord(c) in upper) or (ord(c) in lower) or (ord(c) in digit)):
            frequenciaTexto[c] += 1
    return frequenciaTexto.most_common()


def frequenciaAlfabeto():
    alfabeto = collections.Counter()

    alfabeto['a'] = 14.63
    alfabeto['b'] = 1.04
    alfabeto['c'] = 3.88
    alfabeto['d'] = 4.99
    alfabeto['e'] = 12.57
    alfabeto['f'] = 1.02
    alfabeto['g'] = 1.30
    alfabeto['h'] = 1.28
    alfabeto['i'] = 6.18
    alfabeto['j'] = 0.40
    alfabeto['k'] = 0.02
    alfabeto['l'] = 2.78
    alfabeto['m'] = 4.74
    alfabeto['n'] = 5.05
    alfabeto['o'] = 10.73
    alfabeto['p'] = 2.52
    alfabeto['q'] = 1.2
    alfabeto['r'] = 6.53
    alfabeto['s'] = 7.81
    alfabeto['t'] = 4.34
    alfabeto['u'] = 4.34
    alfabeto['v'] = 1.67
    alfabeto['w'] = 0.01
    alfabeto['x'] = 0.21
    alfabeto['y'] = 0.01
    alfabeto['z'] = 0.47

    return alfabeto.most_common()


def swapChars(entrada, descriptografada):
    saida = ''
    descriptografada2 = ''
    alfa = list(frequenciaAlfabeto())
    beta = list(frequenciaChar(entrada))
    acumulador = list()

    
    for i in range(0, len(beta)):
        acumulador.append(abs(ord(alfa[i][0]) - ord(beta[i][0])))

    chave = max(set(acumulador), key=acumulador.count)
    print("Valor do acumulador: ", acumulador)
    print("Printando a key", chave)
    print("Decifrador de Cesar")
    descriptografada2 = decifradorDeCesar(entrada, chave, descriptografada)
    
    return descriptografada2


def main():
    
    entrada = ''

    
    if len(sys.argv) == 6:
        opt = sys.argv[0]
        escolha = sys.argv[1]
        chave = sys.argv[2]
        key = int(sys.argv[3])
        pathEntrada = sys.argv[4]
        pathSaida = sys.argv[5]
        
        temp = open(pathEntrada, 'r')
        saida = open(pathSaida, 'w')
        
        for linha in temp:
            for c in linha:
                entrada += c
        
        
        
        if opt=='cesar.py' and chave == '-k':
            if escolha== '-c':
                print("Cifrador")
                a =cifradorDeCesar(entrada, key, criptografada)
                      
            elif escolha== '-d':
                print("Decifrador")
                a = decifradorDeCesar(entrada, key, descriptografada)
                
            elif escolha== '-f':
                print("Frequência")
                a = swapChars(entrada, descriptografada)  
            
    
        print(opt)
        print(escolha)
        print(key)
        
        saida.writelines(a)
        saida.close()
        
    
        
    else:
        print("Errou feio, errou rude!")
        

if __name__ == "__main__":
    main()
