#!/usr/bin/env python3

#############################################################
#                                                           #
#                                                           #
#      Trabalho 2 de Segurança e Auditoria de Sistemas      #
#      Autor: Vinícius Sandri Diaz                          #
#                                                           #
#                                                           #
#      data da última modificação: 10/04/18                  #
#      Implementação: Token                                 #
#                                                           #
#                                                           #
#                                                           #
#                                                           #
#############################################################

import hashlib
import time
from time import strftime
#import getpass

listHash = list()
listUser = list()
listSecret = list()
salt = 631137341974615831861173915729
timeTemporizador = 60


def hashGenerator(entrada, salt):

    hash = entrada + str(salt).encode()
    base = hashlib.sha256(hash).hexdigest()
    return base


def hashGeneratorList(tamanho, entrada, salt, tempo):

        listHash.clear()
        hash = str(entrada).encode() + str(salt).encode() + str(tempo).encode()
        base = hashlib.sha256(hash).hexdigest()
        listHash.append(base)

        for i in range(0, (tamanho-1)):
            tmp = hashlib.sha256(listHash[i].encode('utf-8')).hexdigest()
            listHash.append(tmp)


def tokenGenerator():

    listToken.clear()
    for key in listHash:
        token = key[5] + key[15] + key[27] + key[41] + key[57] + key[63]
        listToken.append(token)


def cadastraUsuario():

    userFile = open("users.txt", 'a')
    usuario = input("Usuário: ")
    #semente =  getpass.getpass("Semente: ")
    semente = input("Semente: ")
    hashSemente = hashGenerator(semente.encode(), salt)

    #secret =  getpass.getpass("Senha Local: ")
    secret = input("Senha Local: ")
    hashSecret = hashGenerator(secret.encode(), salt)

    userFile.write(usuario + "," + hashSecret + "," + hashSemente)
    userFile.write("\n")


def login():

    userFile = open("users.txt", 'r')
    for line in userFile:
        campo = line.split(",")
        listUser.append(campo[0])
        listSecret.append(campo[1])
        listSemente.append(campo[2])
    validaUsuario()


def validaUsuario():

    usario = input("Usuario: ")
    #secret = getpass.getpass("Senha: ")
    secret = input("Senha: ")
    hashSecret = hashGenerator(secret.encode(), salt)

    if usario in listUser and hashSecret in listSecret:
        print("Login Efetuado")

        while True:
            entrada = input("Para Gerar Tokens digite 1: ")

            if entrada == '1':
                tempo = strftime("%Y-%m-%d %H:%M", time.gmtime())
                index = listUser.index(usario)
                hashGeneratorList(hashListSize,
                                  str(listSemente[index]).encode(),
                                  salt, tempo)
                tokenGenerator()
                print("Tokens Gerados em: " + tempo + ". Expiram em 1 min")
                for i in range(0, len(listToken)):
                    print(listToken[i])

                temporizador(timeTemporizador)

            elif entrada != '1':
                print("Encerrando Seção do Usuário")
                break

    else:
        print("Falha no Login")


def temporizador(tempo):
    print("Iniciando a contagem de tempo.")
    time.sleep(tempo)
    listToken.clear()
    listHash.clear()
    tempo = strftime("%Y-%m-%d %H:%M", time.gmtime())
    print("Tokens Expiraram às " + tempo)


def main():

    while True:
        entrada = input("1-Novo Usuario; 2-Login; ctr x para encerrar: ")

        if entrada == '\x18':
            break

        elif entrada == '1':
            cadastraUsuario()

        elif entrada == '2':
            login()

        else:
            print("Opção Incorreta. Tente novamente!")

    print("Encerrando Seção")



if __name__ == "__main__":
    main()
