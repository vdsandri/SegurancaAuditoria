#!/usr/bin/env python3

#############################################################
#                                                           #
#                                                           #
#      Trabalho 4 de Segurança e Auditoria de Sistemas      #
#      Autor: Vinícius Sandri Diaz                          #
#                                                           #
#                                                           #
#      data da última modificação: 29/05/18                  #
#      Implementação:                                       #
#                     Cliente                               #
#                                                           #
#                                                           #
#                                                           #
#############################################################


import socket
import sys
from Crypto.Cipher import DES3
import hashlib
import numpy as np
import time
import string
import os

HOST = '127.0.0.1'    # Endereco IP do Servidor
PORT_AS = 6666
PORT_TGS = 7777              # Porta que o Servidor esta
PORT_S = 8888
BUFFER_SIZE = 1024
salt = 895807183804302924969748361977
listUser = {}
serviceList = ('serv1', 'serv2', 'serv3', 'serv4')
msgList = list()
ticketList = list()
listKCTGS = {}
listKCS = {}


def hashGenerator(entrada, salt):

    hash = entrada + str(salt).encode()
    base = hashlib.sha256(hash).hexdigest()
    return base


def cadastraUsuario():

    try:
        userFile = open("idc.txt", 'a')
        usuario = input("Usuário: ")
        key = input("Chave: ")
        hashKey = hashGenerator(key.encode(), salt)

        userFile.write(usuario+"," + hashKey)
        userFile.write("\n")
        time.sleep(0.05)
        userFile.close()
        time.sleep(0.05)
    except Exception as e:
        print("Erro ao abrir e gravar o arquivo idc.txt: ", str(e))


def login():

    listUser.clear()
    try:
        userFile = open("idc.txt", 'r')
        for line in userFile:
            campo = line.split(",")
            listUser[campo[0]] = campo[1]
        time.sleep(0.05)
        userFile.close()
        time.sleep(0.05)
        validaUsuario()

    except Exception as e:
        print("Erro ao abrir o arquivo idc.txt: ", str(e))


def validaUsuario():

    usuario = input("Usuario: ")
    if usuario in listUser:
        print("Login Efetuado")

        entrada = input("Para Solicitar Serviço AS digite 1: ")

        if entrada == '1':
            listKCTGS.clear()
            listKCS.clear()
            a = solicitaAS(usuario)
            time.sleep(0.05)

            if a:
                print("Sucesso no AS!")
                time.sleep(2)
                b = solicitaTGS(usuario)


                if b:
                    print("Sucesso no TGS!")
                    time.sleep(2)
                    c = solicitaServidor(usuario)

                    if c:
                        print("Sucesso no Servidor de Serviço!\n")
                    else:
                        print("Falha no Servidor de Serviço!\n")
                else:
                    print("Falha no TGS!")

            else:
                print("Problemas na Função AS")

        elif entrada != '1':
            print("Encerrando Seção do Usuário")

    else:
        print("Falha no Login")

    msgList.clear()
    ticketList.clear()


def solicitaServidor(usuario):

    tcpS = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print("\nSocket Servidor criado")
    try:
        destS = (HOST, PORT_S)
        tcpS.connect(destS)
        flag = True
    except socket.error:
        print("Binding failed to Servidor")
        flag = False
        sys.exit
    rec_msg = tcpS.recv(BUFFER_SIZE)
    print(rec_msg.decode())

    if len(rec_msg) > 0:
        try:
            cipher = cipherKCS(usuario)
        except Exception as e:
            print("Erro na cipher KCS: ", str(e))
            valida = False
            flag = False
    else:
        valida = False
        flag = False

    if flag:
        try:
            tcpS.send(ticketList[1])
            time.sleep(0.05)
            nbr = np.random.randint(10000)
            campo = msgList[0]
            text = campo.split(",")
            msg = usuario + "," + text[0] + "," + text[1] + "," + str(nbr)
            print("Msg p/ Servidor: ", msg)
            res_encr = encryptText(usuario, msg, cipher)
            tcpS.send(res_encr)
            time.sleep(0.05)

            try:
                rec_msg = tcpS.recv(BUFFER_SIZE)
                if len(rec_msg) > 0:
                    resp = decryptText(usuario, rec_msg, cipher)
                    print("Mensagem Recebida: ", str(resp))
                    valida = validaMsgS(tcpS, msg, str(resp))

            except Exception as e:
                print("Erro no Servidor: ", str(e))
                valida = False

        except Exception as e:
            print("Erro no Servidor: ", str(e))
            valida = False

    tcpS.close()
    return valida


def validaMsgS(tcp, msg1, msg2):
    flag = False
    translator = str.maketrans('', '', string.punctuation)

    cliente = msg1.split(",")
    #resposta = msg2.split("b'")[1]
    #resposta = resposta.replace(resposta[len(resposta)-1], "", 1)
    resposta = msg2.split(",")

    resp = resposta[len(resposta)-1]
    resp = resp.translate(translator)

    print("Resp Teste: ", resp)

    if cliente[len(cliente)-1] == resp:
        msgList.append(msg1)
        msgList.append(msg2)
        flag = True

    else:
        print("Mensagem Inválida")
        print(msg1)
        print(msg2)

    return flag


def solicitaTGS(usuario):

    tcpTGS = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print("\nSocket TGS criado")
    try:
        destTGS = (HOST, PORT_TGS)
        tcpTGS.connect(destTGS)
        flag = True
    except socket.error:
        print("Binding failed to TGS")
        flag = False
        valida = False
        sys.exit
    rec_msg = tcpTGS.recv(BUFFER_SIZE)

    if len(rec_msg) > 0:
        print(rec_msg.decode())
        time.sleep(0.1)
        try:
            cipher = cipherKCTGS(usuario)
        except Exception as e:
            print("Erro na cipher KCTGS: ", str(e))
            valida = False
            flag = False
    else:
        flag = False
        valida = False

    if flag:
        try:
            tcpTGS.send(ticketList[0])
            print("Ticket: ", ticketList[0])
            time.sleep(0.05)
            nbr = np.random.randint(10000)
            campo = msgList[0]
            text = campo.split(",")
            msg = usuario + "," + text[0] + "," + text[1] + "," + str(nbr)
            print("Msg p/ TGS: ", msg)
            res_encr = encryptText(usuario, msg, cipher)
            tcpTGS.send(res_encr)
            time.sleep(0.05)

            try:
                rec_msg = tcpTGS.recv(BUFFER_SIZE)
                if len(rec_msg) > 0:
                    resp = decryptText(usuario, rec_msg, cipher)
                    print("Mensagem Recebida: ", str(resp))
                    valida = validaMsgTGS(tcpTGS, msg, str(resp), usuario)

            except Exception as e:
                print("Erro no TGS: ", str(e))
                valida = False

        except Exception as e:
            print("Erro no TGS: ", str(e))
            valida = False

    tcpTGS.close()
    return valida


def validaMsgTGS(tcp, msg1, msg2, usuario):

    flag = False
    translator = str.maketrans('', '', string.punctuation)
    cliente = msg1.split(",")
    #resposta = msg2.split("b'")[1]
    #resposta = resposta.replace(resposta[len(resposta)-1], "", 1)
    resposta = msg2.split(",")
    resp = resposta[len(resposta)-1]
    resp = resp.translate(translator)

    print("Resp Teste: ", resp)

    if cliente[len(cliente)-1] == resp:
        msgList.append(msg1)
        msgList.append(msg2)
        listKCS[usuario] = resposta[0]

        try:
            rec_msg = tcp.recv(BUFFER_SIZE)
            print("Ticket AS: ", str(rec_msg))
            ticketList.append(rec_msg)
            flag = True

        except Exception as e:
            print("Erro no valida MSG do TGS: ", str(e))

    else:
        print("Mensagem Inválida")
        print(msg1)
        print(msg2)

    return flag


def solicitaAS(usuario):

    tcpAS = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print("\nSocket AS criado")
    try:
        destAS = (HOST, PORT_AS)
        tcpAS.connect(destAS)
        flag = True
    except socket.error:
        print("Binding failed to AS")
        flag = False
        sys.exit
    rec_msg = tcpAS.recv(BUFFER_SIZE)
    respValida = False

    nbr = np.random.randint(10000)
    print(rec_msg.decode())
    cipher = createCipher(usuario)

    while flag:

        swp = input("Serviço Pretendido (1-4): ")

        if(swp >= '0' and swp <= '4'):
            swp = int(swp)
            service = serviceList[swp - 1]
            print("Service: " + service)
            tempo = input("Tempo desejado para o serviço (no máximo 5 min): ")

            if(tempo >= '0' and tempo <= '5'):
                flag = False
                text = service + "," + tempo + "," + str(nbr)
                print("Mensagem p/ AS: ", text)
                msg = encryptText(usuario, text, cipher)
                tcpAS.send(usuario.encode("utf-8"))
                break
            else:
                print("Tempo Inválido!")
        else:
            print("Serviço Inválido!")

    try:
        rec_msg = tcpAS.recv(BUFFER_SIZE)
        print("Resposta AS: ", rec_msg.decode())

        if usuario == rec_msg.decode()[20:]:
            tcpAS.send(msg)
            rec_msg = tcpAS.recv(BUFFER_SIZE)
            print("Resposta AS Cript: ", rec_msg)
            resp = decryptText(usuario, rec_msg, cipher)
            respValida = validaMsgAS(tcpAS, text, str(resp), usuario)

    except Exception as e:
        print("Erro no AS: ", str(e))
        respValida = False

    tcpAS.close()
    return respValida


def validaMsgAS(tcpAS, msg1, msg2, usuario):

    flag = False
    translator = str.maketrans('', '', string.punctuation)
    tcp = tcpAS
    cliente = msg1.split(",")
    resposta = msg2.split(",")

    resp = resposta[len(resposta)-1]
    resp = resp.translate(translator)

    print("Resp Teste: ", resp)

    if cliente[len(cliente)-1] == resp:
        msgList.append(msg1)
        msgList.append(msg2)
        listKCTGS[usuario] = resposta[0]

        try:
            rec_msg = tcp.recv(BUFFER_SIZE)
            print("Ticket AS: ", str(rec_msg))
            ticketList.append(rec_msg)
            flag = True

        except Exception as e:
            print("Erro no valida MSG do AS: ", str(e))

    else:
        print("Mensagem Inválida do AS")
        print(msg1)
        print(msg2)

    return flag


def createCipher(usuario):

    key = listUser.get(usuario)
    key32 = key.ljust(24)[:24]
    iv = key.ljust(16)[35:43]
    cipher = DES3.new(key32, DES3.MODE_CFB, iv)
    return cipher


def cipherKCS(usuario):

    try:
        if usuario in listKCS:
            translator = str.maketrans('', '', string.punctuation)
            print("Lista KCS: ", listKCS)
            key = listKCS[usuario]
            key = key.split("b'")[1]
            key = key.translate(translator)
            print("Hei Key KCS: ", key)
            key32 = key.ljust(24)[:24]
            iv = key.ljust(16)[35:43]
            cipher = DES3.new(key32, DES3.MODE_CFB, iv)
            return cipher
        else:
            print("Cipher KCS: ", str(cipher))
    except Exception as e:
        print("Erro ao abrir o arquivo kcs.txt: ", str(e))


def cipherKCTGS(usuario):
    try:
        print("Lista KCTGS: ", listKCTGS)
        if usuario in listKCTGS:
            translator = str.maketrans('', '', string.punctuation)
            key = listKCTGS[usuario]
            key = key.split("b'")[1]
            key = key.translate(translator)
            print("Hei Key KCTGS: ", key)
            key32 = key.ljust(24)[:24]
            iv = key.ljust(16)[35:43]
            cipher = DES3.new(key32, DES3.MODE_CFB, iv)
            return cipher
        else:
            print("Lista Vazia!")
        print("Cipher KCTGS: ", str(cipher))

    except Exception as e:
        print("Erro na geração do cipherKCTGS: ", str(e))


def encryptText(usuario, mensagem, cipher):
    cipher = cipher
    space = '        '
    text = mensagem
    plaintext = space + text
    msg = cipher.encrypt(plaintext.encode(encoding='utf_8'))
    print("encrypt: " + str(msg))
    return msg


def decryptText(usuario, mensagem, cipher):
    cipher = cipher
    msg = cipher.decrypt(mensagem)
    print("Msg decrypt: ", str(msg[8:]))
    return msg[8:]


def clear():
    os.system('cls' if os.name == 'nt' else 'clear')


def main():
    while True:
        entrada = input("1-Login; 2-Novo Usuario; 3-Clear; ctr x para encerrar: ")

        if entrada == '\x18':
            break

        elif entrada == '2':
            cadastraUsuario()
            time.sleep(0.05)

        elif entrada == '1':
            login()

        elif entrada == '3':
            clear()

        else:
            print("Opção Incorreta. Tente novamente!")

    print("Encerrando Seção")


if __name__ == "__main__":
    main()
