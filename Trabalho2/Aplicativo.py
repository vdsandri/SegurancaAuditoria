#!/usr/bin/env python3

#############################################################
#                                                           #
#                                                           #
#      Trabalho 2 de Segurança e Auditoria de Sistemas      #
#      Autor: Vinícius Sandri Diaz                          #
#                                                           #
#                                                           #
#      data da última modificação: 10/04/18                  #
#      Implementação: Token                                 #
#                                                           #
#                                                           #
#                                                           #
#                                                           #
#############################################################

import hashlib
import time
from time import strftime
from interruptingcow import timeout
import getpass

listToken = list()
listHash = list()
listUser = list()
listSecret = list()
listSemente = list()
salt = 631137341974615831861173915729
hashListSize = 5
timeTemporizador = 60


def hashGenerator(entrada, salt):

    hash = entrada + str(salt).encode()
    base = hashlib.sha256(hash).hexdigest()
    return base


def hashGeneratorList(tamanho, entrada, salt, tempo):

        listHash.clear()
        hash = str(entrada).encode() + str(salt).encode() + str(tempo).encode()
        base = hashlib.sha256(hash).hexdigest()
        listHash.append(base)

        for i in range(0, (tamanho-1)):
            tmp = hashlib.sha256(listHash[i].encode('utf-8')).hexdigest()
            listHash.append(tmp)


def tokenGenerator():

    listToken.clear()
    for key in listHash:
        token = key[5] + key[15] + key[27] + key[41] + key[57] + key[63]
        listToken.append(token)


def login():

    userFile = open("users.txt", 'r')
    for line in userFile:
        campo = line.split(",")
        listUser.append(campo[0])
        listSecret.append(campo[1])
        listSemente.append(campo[2])
    validaUsuario()


def validaUsuario():

    usario = input("Usuario: ")

    if usario in listUser:
        print("Usuário Válido.")
        tempo = strftime("%Y-%m-%d %H:%M", time.gmtime())
        index = listUser.index(usario)
        hashGeneratorList(hashListSize, str(listSemente[index]).encode(),
                          salt, tempo)
        tokenGenerator()
        print("\nTokens Gerados às " + tempo)
        try:
            with timeout(timeTemporizador, exception=RuntimeError):
                while True:

                    entrada = input("Digite o Token: ")
                    if entrada == '\x18':
                        break

                    elif entrada in listToken:
                        print("Chave Válida")
                        a = listToken.index(entrada)
                        listToken.pop(a)

                        passo = len(listToken) - a

                        for i in range(0, passo):
                            listToken.pop()

                        print("Tokens Válidos", len(listToken))

                    else:
                        print("ERRO! Chave Inválida")
        except RuntimeError:
            temporizador()
            pass

    else:
        print("Usuário Inválido")


def temporizador():
    listToken.clear()
    listHash.clear()
    tempo = strftime("%Y-%m-%d %H:%M", time.gmtime())
    print("\n\nTokens Expiraram às " + tempo)
    print("Reiniciando a Sessão")


def main():

    while True:
        entrada = input("1-Login; ctr x para encerrar: ")

        if entrada == '\x18':
            break

        elif entrada == '1':
            login()

        else:
            print("Opção Incorreta. Tente novamente!")

    print("Encerrando Seção")


if __name__ == "__main__":
    main()
