#!/usr/bin/env python3

#############################################################
#                                                           #
#                                                           #
#      Trabalho 1 de Segurança e Auditoria de Sistemas      #
#      Autor: Vinícius Sandri Diaz                          #
#                                                           #
#                                                           #
#      data da última modificação: 23/03/18                  #
#      Implementação:                                       #
#                     Cifra de Cesar                        #
#                                                           #
#                                                           #
#                                                           #
#############################################################

import numpy as np
import collections
import sys



upper = range(65, 91)
lower = range(97, 123)
digit = range(48, 58)

rotation = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'

entrada = 'TestaNDO a ?criptoGRAfia de cEsaR NestE dia CHUvoso de CuRITibA! S2<=>@'

entrada2 = 'g5Bt5 t54yvtz3v4A5 wrG t53 7Bv r9 6v995r9 9v 9z4Ar3 58xB2y59r9. dBzA5 t54yvtz3v4A5, 7Bv 9v 9z4Ar3 yB3z2uv9. Vy r99z3 7Bv r9 v96zxr9 9v3 x8r59 v8xBv3 uv9uv4y59r3v4Av r trsvtr 6r8r 5 tvB, v47Br4A5 r9 tyvzr9 r9 srzEr3 6r8r r Av88r, 9Br 3rv. cv54r8u5 Ur mz4tz.'

criptografada = ''
descriptografada = ''
frequenciaTexto = collections.Counter()

#key = 1000


def cifradorDeCesar(entrada, key, criptografada):
    for c in entrada:
        swp = rotation.find(c)
        
        if c != ' ' and ((ord(c) in upper) or (ord(c) in lower) or (ord(c) in digit)):
            
            criptografada += rotation[((swp + key) % len(rotation))]
     
        else:
            criptografada +=  c
        
    print("Msg Original:         ", entrada)
    print("Msg criptografada:    ", criptografada)
    return criptografada
    

def decifradorDeCesar(entrada, key, descriptografada):
    
    for c in entrada:
        swp = rotation.find(c)
       
        if c != ' ' and ((ord(c) in upper) or (ord(c) in lower) or (ord(c) in digit)):
            
            descriptografada += rotation[((swp - key) % len(rotation))]
                        
        else:
            descriptografada +=  c
    
    print("Msg descriptografada: ", descriptografada)


def frequenciaChar(entrada):
    a = entrada.lower()
    frequenciaTexto.clear()

    for c in a:
        if c in a and c != ' ' and ((ord(c) in upper) or (ord(c) in lower) or (ord(c) in digit)):
            frequenciaTexto[c] += 1
    return frequenciaTexto.most_common()


def frequenciaAlfabeto():
    alfabeto = collections.Counter()

    alfabeto['a'] = 14.63
    alfabeto['b'] = 1.04
    alfabeto['c'] = 3.88
    alfabeto['d'] = 4.99
    alfabeto['e'] = 12.57
    alfabeto['f'] = 1.02
    alfabeto['g'] = 1.30
    alfabeto['h'] = 1.28
    alfabeto['i'] = 6.18
    alfabeto['j'] = 0.40
    alfabeto['k'] = 0.02
    alfabeto['l'] = 2.78
    alfabeto['m'] = 4.74
    alfabeto['n'] = 5.05
    alfabeto['o'] = 10.73
    alfabeto['p'] = 2.52
    alfabeto['q'] = 1.2
    alfabeto['r'] = 6.53
    alfabeto['s'] = 7.81
    alfabeto['t'] = 4.34
    alfabeto['u'] = 4.34
    alfabeto['v'] = 1.67
    alfabeto['w'] = 0.01
    alfabeto['x'] = 0.21
    alfabeto['y'] = 0.01
    alfabeto['z'] = 0.47

    return alfabeto.most_common()


def swapChars(entrada):
    saida = ''
    alfa = list(frequenciaAlfabeto())
    beta = list(frequenciaChar(entrada))
    acumulador = list()

    
    for i in range(0, len(beta)):
        acumulador.append(abs(ord(alfa[i][0]) - ord(beta[i][0])))

    chave = max(set(acumulador), key=acumulador.count)
    print("Valor do acumulador: ", acumulador)
    print("Printando a key", chave)
    print("Decifrador de Cesar")
    decifradorDeCesar(entrada, chave, descriptografada)


def main():
    
    if len(sys.argv) == 6:
        opt = sys.argv[1]
        escolha = sys.argv[2]
        key = int(sys.argv[3])
        entrada = sys.argv[4]
        saida = sys.argv[5]
        
        if opt=='cesar':
            if escolha== '-c':
                print("Cifrador")
                cifradorDeCesar(entrada, key, criptografada)
                      
            elif escolha== '-d':
                print("Decifrador")
                decifradorDeCesar(entrada, key, descriptografada)
            
    
        print(opt)
        print(escolha)
        print(key)
        
    else:
        print("Errou feio, errou rude!")
        
#a = cifradorDeCesar(entrada, key, criptografada)
#swapChars(entrada2)


if __name__ == "__main__":
    main()
